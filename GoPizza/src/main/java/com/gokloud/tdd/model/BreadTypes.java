package com.gokloud.tdd.model;

import java.math.BigDecimal;

public class BreadTypes {

	private String name;

	private BigDecimal price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BreadTypes(String name, BigDecimal price) {
		super();
		this.name = name;
		this.price = price;
	}

	public BreadTypes() {
		super();
	}

}
