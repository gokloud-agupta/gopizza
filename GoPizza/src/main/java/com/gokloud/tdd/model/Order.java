package com.gokloud.tdd.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "order")
public class Order {

	@Id
	private String id;

	private List<Pizza> pizzas;

	private List<PizzaToppings> toppings;

	private BreadTypes bread;

	private String totalPrice;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	public List<PizzaToppings> getToppings() {
		return toppings;
	}

	public void setToppings(List<PizzaToppings> toppings) {
		this.toppings = toppings;
	}

	public BreadTypes getBread() {
		return bread;
	}

	public void setBread(BreadTypes bread) {
		this.bread = bread;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

}
