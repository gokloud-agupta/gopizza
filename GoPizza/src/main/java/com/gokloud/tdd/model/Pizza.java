package com.gokloud.tdd.model;

import java.math.BigDecimal;
import java.util.List;

public class Pizza {

	private String id;

	private String name;

	private BigDecimal price;

	private List<String> toppingsIncluded;

	private String desc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public List<String> getToppingsIncluded() {
		return toppingsIncluded;
	}

	public void setToppingsIncluded(List<String> toppingsIncluded) {
		this.toppingsIncluded = toppingsIncluded;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Pizza(String name, BigDecimal price, List<String> toppingsIncluded, String desc) {
		super();
		this.name = name;
		this.price = price;
		this.toppingsIncluded = toppingsIncluded;
		this.desc = desc;
	}

	public Pizza() {
		super();
	}

}
