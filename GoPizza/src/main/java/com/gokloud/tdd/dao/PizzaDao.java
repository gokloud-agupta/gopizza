package com.gokloud.tdd.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.gokloud.tdd.model.Pizza;

public interface PizzaDao extends MongoRepository<Pizza, String> {

}
