package com.gokloud.tdd.serviceimpl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.gokloud.tdd.constants.MessageConstants;
import com.gokloud.tdd.exceptions.EntityNotFoundException;
import com.gokloud.tdd.model.BreadTypes;
import com.gokloud.tdd.model.Pizza;
import com.gokloud.tdd.model.PizzaToppings;
import com.gokloud.tdd.service.UserService;

@Service
public class UserServiceImpl extends MessageConstants implements UserService {

	private static List<Pizza> getPizzas() {
		List<String> veggieSupremeToppingsIncluded = Arrays
				.asList("Cheese,chicken, capsicum, onion, red paprika, mushroom");
		Pizza veggieSupremePizza = new Pizza("Veggie Supreme", new BigDecimal(250), veggieSupremeToppingsIncluded, "");
		List<String> chickenSupremeToppingsIncluded = Arrays.asList("Cheese,chicken, meatball");
		Pizza chickenSupremePizza = new Pizza("Chicken Supreme", new BigDecimal(300), chickenSupremeToppingsIncluded,
				"");
		List<String> doubleChickenFeastToppingsIncluded = Arrays.asList("Cheese,chicken, capsicum, onion, red paprika");
		Pizza doubleChickenFeastPizza = new Pizza("Double Chicken feast", new BigDecimal(350),
				doubleChickenFeastToppingsIncluded, "");
		List<String> vegFarmhouoseToppingsIncluded = Arrays
				.asList("Cheese,capsicum, onion, red paprika, black olives, corn");
		Pizza vegFarmhouosePizza = new Pizza("Veg Farmhouse", new BigDecimal(300), vegFarmhouoseToppingsIncluded, "");
		List<String> margaritaToppingsIncluded = Arrays.asList("Cheese");
		Pizza margaritaPizza = new Pizza("Margarita", new BigDecimal(225), margaritaToppingsIncluded, "");
		return Arrays.asList(veggieSupremePizza, chickenSupremePizza, doubleChickenFeastPizza, vegFarmhouosePizza,
				margaritaPizza);
	}

	private static List<PizzaToppings> getPizzaToppings() {
		PizzaToppings cheese = new PizzaToppings("Cheese", new BigDecimal(20));
		PizzaToppings olive = new PizzaToppings("Olives", new BigDecimal(20));
		PizzaToppings onion = new PizzaToppings("Onion", new BigDecimal(10));
		PizzaToppings chicken = new PizzaToppings("Chicken", new BigDecimal(25));
		PizzaToppings cottageChicken = new PizzaToppings("Cottage cheese", new BigDecimal(15));
		return Arrays.asList(cheese, olive, onion, chicken, cottageChicken);
	}

	private static List<BreadTypes> getPizzaBreadTypes() {
		BreadTypes regular = new BreadTypes("Regular ", new BigDecimal(0));
		BreadTypes thinCrust = new BreadTypes("Thin crust", new BigDecimal(20));
		BreadTypes cheezeCrust = new BreadTypes("Cheese crust", new BigDecimal(25));
		BreadTypes pan = new BreadTypes("Pan", new BigDecimal(10));
		return Arrays.asList(regular, thinCrust, cheezeCrust, pan);
	}

	public static Object main(String[] args) {
		List<String> input = Arrays.asList(args);
		List<Pizza> pizzas = getPizzas();
		List<PizzaToppings> pizzaToppings = getPizzaToppings();
		List<BreadTypes> pizzaBreads = getPizzaBreadTypes();
		BigDecimal orderPrice = new BigDecimal(0);
		for (int i = 0; i < input.size(); i++) {
			String[] pizzaOrder = input.get(i).split(COMMA);
			Pizza pizzaDetail = pizzas.stream().filter(pizza -> pizzaOrder[0].equalsIgnoreCase(pizza.getName()))
					.findAny().orElseThrow(() -> new EntityNotFoundException(PIZZA_NOT_FOUND));
			orderPrice = orderPrice.add(pizzaDetail.getPrice());
			for (int j = 1; j < pizzaOrder.length; j++) {
				final int k = j;
				PizzaToppings pizzaTopping = pizzaToppings.stream()
						.filter(topping -> pizzaOrder[k].trim().equalsIgnoreCase(topping.getName().trim())).findAny()
						.orElse(null);
				if (pizzaTopping != null) {
					if (pizzaOrder[k].contains(HIPHEN)) {
						orderPrice = orderPrice.subtract(pizzaTopping.getPrice());
					} else {
						orderPrice = orderPrice.add(pizzaTopping.getPrice());
					}
				} else {
					BreadTypes breadType = pizzaBreads.stream()
							.filter(bread -> pizzaOrder[k].trim().equalsIgnoreCase(bread.getName().trim())).findAny()
							.orElse(null);
					if (breadType != null) {
						if (pizzaOrder[k].contains(HIPHEN)) {
							orderPrice = orderPrice.subtract(breadType.getPrice());
						} else {
							orderPrice = orderPrice.add(breadType.getPrice());
						}
					}
				}
			}
		}
		System.out.println("Final bill price is : " + orderPrice);
		return orderPrice;
	}
}
