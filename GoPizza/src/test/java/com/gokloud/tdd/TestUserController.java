package com.gokloud.tdd;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.gokloud.tdd.serviceimpl.UserServiceImpl;

import java.math.BigDecimal;


@ExtendWith(MockitoExtension.class)
public class TestUserController {

	@Mock
	private UserServiceImpl userService;

	@InjectMocks
	UserServiceImpl userServiceImpl = new UserServiceImpl();

	@Test
	public void testPizzaOrder() {
		String[] args = new String[] { "Chicken Supreme, Cheese, olives, Thin Crust", "Veggie Supreme, cheese, -onion" };
		assertEquals(new BigDecimal("630"), userServiceImpl.main(args));
	}

}
